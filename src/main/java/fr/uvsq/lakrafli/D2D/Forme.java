package fr.uvsq.lakrafli.D2D;

import java.awt.Point;
import java.io.Serializable;

public abstract class Forme implements Serializable{

	protected Point origine;
	public Forme(Point o){
		origine=new Point(o);
	}
	public abstract void Afficher();
	public abstract void Deplacer(Point p);
	


}
