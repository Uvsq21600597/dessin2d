package fr.uvsq.lakrafli.D2D;

import java.awt.Point;

public class Rectangle  extends Forme {

	private int  largeur ;
	private int  longueur ;
	
	public Rectangle(Point o ,int largeur,int  longueur) throws DessinException {
		super(o);
		
		
		if ( largeur!=0  ) this.largeur=largeur;
		if ( longueur!=0 ) this.longueur=longueur;
			
		else throw new DessinException("Erreur : Largeur ou Longeur doit être Superieur à 0");

	}

	
	@Override
	public String toString() {
		return "Rectangle [largeur=" + largeur + ", longueur=" + longueur + "]";
	}


	@Override
	public void Afficher() {
		System.out.println("Rectangle [X:" + origine.x + ", Y:" + origine.y +", largeur :" +
							largeur +", longueur:" + longueur +"]");
		
	}

	@Override
	public void Deplacer(Point p) {
		// TODO Auto-generated method stub
		origine.setLocation(origine.getX()+p.getX(), origine.getY()+p.getY());
		
	}

	
	

}
