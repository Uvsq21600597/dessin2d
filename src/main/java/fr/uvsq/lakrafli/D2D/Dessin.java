package fr.uvsq.lakrafli.D2D;

import java.awt.Point;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Vector;

	public class Dessin implements  Serializable  {
		private Vector<Forme> Formes ;
		private Fenetre fenetre;
	
		
		
	public Dessin(){
			Formes = new Vector<Forme>();
			fenetre= new Fenetre(1000);
	}
	
	
	public Vector<Forme> getFormes() {
		return Formes;
	}
	
	
	public void setFormes(Vector<Forme> formes) {
		Formes = formes;
	}	
	
	public void ChargerAll() {
		Formes = new Vector<Forme>();
			   try {
		         FileInputStream fileIn = new FileInputStream("Object.ser");
		         ObjectInputStream in = new ObjectInputStream(fileIn);
		       
		         Vector<Forme> f = new Vector<Forme>();
		         
		          f = (Vector<Forme>) in.readObject();
		         this.Formes=f;
	
		          in.close();
		         fileIn.close();
		         
		      } catch (IOException i) {
		         i.printStackTrace();
		         return;
		      } catch (ClassNotFoundException c) {
		         System.out.println("le Ficher est bien chargée");
		         c.printStackTrace();
		         return;
		      }
		}
	
	     
	public void SauvgarderAll() {
		  		
		  		 try {
		  	         FileOutputStream fileOut = new FileOutputStream("Object.ser");
		  	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
		  	         out.writeObject(this.Formes);
		  	         out.close();
		  	         fileOut.close();
		  	         System.out.println("Formes Bien Enregistrer ");
		  	      	 
		  		 	} catch (IOException i) {
		  	      						i.printStackTrace();
		  	      								}
		  					}
	
	
	
	public void Ajouter(Forme f) throws DessinException{
	
		if(fenetre.isContain(f))Formes.add(f);
		else throw new DessinException("Impossible d'ajouter");
	}
		
	
	public void Afficher(){
		
		for (int i = 0; i < Formes.size(); i++) {
			Formes.get(i).Afficher();
			System.out.println();
			
		}
		
	}
	public void Deplacer(Point point) throws DessinException{
		
		for (int i = 0; i < Formes.size(); i++) {
			Formes.get(i).Deplacer(point);
		
		}
		
	}

}
