package fr.uvsq.lakrafli.D2D;
import java.awt.Point;
import java.io.Serializable;

public class Cercle extends Forme implements Serializable {

	private int rayon;
	
	public Cercle(Point  o, int rayon) throws DessinException
	{
		super(o);
		if (rayon!=0) this.rayon=rayon;
		else throw new DessinException("Erreur : racine doit être Superieur à 0");
	}
	
	
	@Override
	public String toString() {
		return "Cercle [rayon=" + rayon + ", origine=" + origine + "]";
	}


	@Override
	public void Afficher() {
		// TODO Auto-generated method stub
		System.out.println("Cercle [X:" + origine.x + ", Y:" + origine.y +", Rayon :" + rayon +"]");

	}

	@Override
	public void Deplacer(Point p) {
		// TODO Auto-generated method stub
		origine.setLocation(origine.getX()+p.getX(), origine.getY()+p.getY());
		
		
	}
	

}
