package fr.uvsq.lakrafli.D2D;

import java.awt.Point;
import java.io.Serializable;

public class Fenetre implements Serializable{
		private int taille;
		private Point p;
		
		public Fenetre(int taille) {
			p=new Point(0,0);
			this.taille=taille;
		}
		
		public boolean isContain(Forme f){
			return f.origine.x> 0 && f.origine.y>0 && f.origine.x< taille && f.origine.y<taille; 
		}
}
