package fr.uvsq.lakrafli.D2D;

import java.awt.Point;
import java.util.Scanner;


public class Main {
	
	public static void main(String[] args) throws DessinException {
		
		Dessin dessin=new Dessin();
		Scanner sc=new Scanner(System.in); 
		String cmd;
		int x;
		int y;
		dessin.ChargerAll();
		do{
			System.out.println(">> Tapez : ");
			System.out.println("* ' D ' pour dessiner une forme ");
			System.out.println("* ' M ' pour deplacer Toutes les Formes  ");
			System.out.println("* ' A ' pour afficher Toutes les Formes  ");
			System.out.print(">>");
			cmd= sc.nextLine();
			if(cmd.equals("D")){
				System.out.println("* Pour dessiner une cercle  tapez:  C  ");
				System.out.println("* Pour dessiner une Rectangle  tapez:  R  ");
				System.out.print(">>");
				cmd= sc.nextLine();
				System.out.println(">> Position:");
				System.out.print("X :");
				x= Integer.parseInt(sc.nextLine());
				System.out.print("Y :");
				y= Integer.parseInt(sc.nextLine());
				
				if (cmd.equals("R")){
					
					int largeur,longueur;
					System.out.print("largeur :");
					largeur= Integer.parseInt(sc.nextLine());
					System.out.print("longueur :");
					longueur = Integer.parseInt(sc.nextLine());
					try {
						dessin.Ajouter(new Rectangle(new Point(x,y), largeur,longueur ));
					} catch (DessinException e) {
						System.err.println(e.getCause());
					}
					
				}
				if (cmd.equals("C")){
					int rayon;
					System.out.print("Saisir le rayon :");
					rayon = Integer.parseInt(sc.nextLine());
					try {
						dessin.Ajouter(new Cercle(new Point(x,y), rayon ));
					} catch (DessinException e) {
						System.err.println(e.getCause());
					}
				}
			}
			
			if (cmd.equals("M")){
				System.out.println(">> Traget:");
				System.out.print("X :");
				x= Integer.parseInt(sc.nextLine());
				System.out.print("Y :");
				y= Integer.parseInt(sc.nextLine());
				try {
					dessin.Deplacer(new Point(x, y));
				} catch (DessinException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (cmd.equals("A")){
				dessin.Afficher();
			}
			System.out.println();
		}while(!cmd.equals("exit"));
		dessin.SauvgarderAll();
		
		
		/*
		Dessin d = new Dessin();
		//d.ChargerAll();
		/*System.out.println(d.getFormes().toString());
		Point p1= new Point(22, 22);
		Point p2= new Point(33, 33);
		Point p4= new Point(100, 100);
		Point f= new Point(1000, 1000);
		Point erreur= new Point(12000, 10200);
		
		Cercle c1 = new Cercle(p1,23);
		//Cercle c2 = new Cercle(p2,33);
		//Cercle c3 = new Cercle(p4,43);
		
		
		Rectangle r1= new Rectangle(p2,100,20);
		//d.Ajouter(c3);
		//d.Ajouter(c2);
		d.Ajouter(c1);
		d.SauvgarderAll();*/
	}
		
}
