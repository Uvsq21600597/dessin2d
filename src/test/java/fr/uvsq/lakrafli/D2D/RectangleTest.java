package fr.uvsq.lakrafli.D2D;

import static org.junit.Assert.*;

import java.awt.Point;

import org.junit.Test;

public class RectangleTest {

	  @Test
      public void DeplacerRectangleTest() throws DessinException{
    	  Rectangle r1 = new Rectangle(new Point(10,10), 30,30);
    	  Rectangle excepted = new Rectangle(new Point(10,10), 30,30);
    	  r1.Deplacer(new Point(10,10));
    	  assertEquals(excepted.toString(), r1.toString() );
      }
	
      
      @Test(expected = DessinException.class)
      public void RectangleException() throws DessinException
  	{
  		Rectangle r1 = new Rectangle(new Point(10,10), 0,0);
  		
  	}
}
