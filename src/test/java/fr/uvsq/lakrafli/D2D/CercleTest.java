package fr.uvsq.lakrafli.D2D;


import java.awt.Point;

import org.junit.Test;

import junit.framework.TestCase;

public class CercleTest extends TestCase {
     
      
      @Test
      public void deplacerCercleTest() throws DessinException{
    	  Cercle c1 = new Cercle(new Point(10,10), 23);
    	  Cercle excepted = new Cercle(new Point(20,20), 23);
    	  c1.Deplacer(new Point(10,10));
    	  assertEquals(excepted.toString(), c1.toString() );
      }
	
      
      @Test(expected = DessinException.class)
      public void CercleException() throws DessinException
  	{
  		Cercle c1 = new Cercle(new Point(10,10), -23);
  		
  	}
      
}
